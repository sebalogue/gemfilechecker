require 'humanize'

class Chopper
  def chop(elem, array)
    return -1 if array.empty?

    array.find_index(elem)
  end

  def sum(array)
    return 'vacio' if array.empty?

    num_result = array.inject { |sum, n| sum + n }
    numbers_arr = num_result.digits.reverse!
    return 'demasiado grande' if numbers_arr.length > 2

    r = []
    numbers_arr.each do |i|
      r.push(i.humanize(locale: :es))
    end
    r.join(',')
  end
end
