# agregar: 1. que diga por q es invalido
#          2. agregar prueba para chequear orden de gems (unitario y de aceptacion)

require 'rspec'
require_relative '../model/validator'

describe Validator do
  subject { @validator = described_class.new }

  it { is_expected.to respond_to :process }

  it 'should validate gemfile string and return "Error: Gemfile vacio"' do
    gemfile_string = ''
    result = described_class.new.process gemfile_string
    expect(result).to eq INVALID_EMPTY
  end

  it 'should return "Gemfile correcto" when valid Gemfile' do
    gemfile_content = File.read "#{__dir__}/samples/Gemfile.valid"
    validation_result = described_class.new.process gemfile_content
    expect(validation_result).to eq VALID
  end

  it 'should return "Gemfile vacio" when empty Gemfile' do
    gemfile_content = File.read "#{__dir__}/samples/Gemfile.invalid.empty"
    validation_result = described_class.new.process gemfile_content
    expect(validation_result).to eq INVALID_EMPTY
  end

  it 'should return "Error: Gemfile sin source" when no source Gemfile' do
    gemfile_content = File.read "#{__dir__}/samples/Gemfile.invalid.source"
    validation_result = described_class.new.process gemfile_content
    expect(validation_result).to eq INVALID_SOURCE
  end

  it 'should return "Error: Gemfile sin version" when no ruby version Gemfile' do
    gemfile_content = File.read "#{__dir__}/samples/Gemfile.invalid.rb_version"
    validation_result = described_class.new.process gemfile_content
    expect(validation_result).to eq INVALID_VERSION
  end

  it 'should return "Error: Gems desordenadas" when unordered gems' do
    gemfile_content = File.read "#{__dir__}/samples/Gemfile.invalid.unordered"
    validation_result = described_class.new.process gemfile_content
    expect(validation_result).to eq INVALID_UNORDERED
  end
end
