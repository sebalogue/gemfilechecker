#!/bin/bash

set -e

function check() {
  ruby app.rb "$2"  | grep "$3" >/dev/null && echo "$1:ok" || echo "$1:error"            
}   

echo 'Running acceptance tests for gchecker'
check '01' 'spec/samples/Gemfile.valid' 'Gemfile correcto'
check '02' 'spec/samples/Gemfile.invalid.empty' 'Error: Gemfile vacio'
check '03' 'spec/samples/Gemfile.invalid.source' 'Error: Gemfile sin source'
check '04' 'spec/samples/Gemfile.invalid.rb_version' 'Error: Gemfile sin version'
check '05' 'spec/samples/Gemfile.invalid.unordered' 'Error: Gems desordenadas'
