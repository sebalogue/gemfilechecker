VALID = 'Gemfile correcto'.freeze
INVALID_EMPTY = 'Error: Gemfile vacio'.freeze
INVALID_SOURCE = 'Error: Gemfile sin source'.freeze
INVALID_VERSION = 'Error: Gemfile sin version'.freeze
INVALID_UNORDERED = 'Error: Gems desordenadas'.freeze

class Validator
  def process(gemfile_content)
    return INVALID_EMPTY if gemfile_content.empty?

    source_idx = gemfile_content.index('source')
    return INVALID_SOURCE unless source_idx

    ruby_version_idx = gemfile_content.index('ruby ')
    return INVALID_VERSION unless ruby_version_idx

    gemfile_lines = gemfile_content.lines
    gemfile_lines.each do |line|
      gemfile_lines.delete(line) if line[0..3] != 'gem '
    end
    ordered_gems = gemfile_lines.each_cons(2).all? { |p, n| (p <=> n) != 1 }
    return INVALID_UNORDERED unless ordered_gems

    VALID
  end
end
